# Instructions 
1. Create the wordpress database 
`CREATE DATABASE {database_name} CHARACTER SET utf8 COLLATE utf8_general_ci;`
2. Create a user for the database 
`GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON {database_name}.* TO '{datbase_name}'@'localhost' IDENTIFIED BY '{an_appropriate_password}';`




## Appendix 
- When you see `{some_text}` you should replace it with the appropriate thing
- Feel free to make `localhost` whatever appropriate server address/name (vhosts?)  
