<?php 
/**
 * The header for the example theme
 * 
 */
?> 
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width" />
        <title><?php wp_title( '|', true, 'right'); ?></title>
        <!-- HTML5 SHIV for IE --><!-- If using Modernizr you can remove this script! -->
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header class="site-header">
            <h1>
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/logo.jpg" alt="logo" />
                </a>
            </h1>
        </header>
        <nav class="main-navigation">
            <?php 
            $nav_menu_args = array(
                'theme_location' => 'primary', 
                'container' => false,
            );
            wp_nav_menu($nav_menu_args); 
            ?>
        </nav>
