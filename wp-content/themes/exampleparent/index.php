<?php 
/**
 * Most generic template for the theme 
 */
?>

<?php get_header(); ?>
    <section class="content-area">
        <?php if ( have_posts() ) : ?>
            <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
        <?php endif; ?> 
        <!-- Start the loop -->
        <?php while ( have_posts() ) : the_post(); ?> 
            <?php get_template_part( 'content', get_post_format() ); ?>
        <?php endwhile; ?>
    </section>
<?php get_footer(); ?> 