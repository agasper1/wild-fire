<?php
/**
 * Example Parent functions and definitions
 * 
 * @package example-wordpress
 */

// only call init functions of the function does not already exist
 if ( ! function_exists('exampleparent_setup' ) ) {
     function exampleparent_setup() {
         /**
          * Add default posts and comments RSS 
          */
          add_theme_support( 'automatic-feed-links' );
          /**
           * Enable support for Post Thumbnails 
           */
          add_theme_support( 'post-thumbnails' );

          /**
           * Enable support for post formats
           */
          add_theme_support( 'post-formats', array('aside', 'image', 'video', 'quote', 'link') );

          /**
           * Register custom menus
           */
          exampleparent_register_custom_menus();

          /**
           * Register custom blog post type 
           */

           example_parent_create_blog_post_type();

     }
 }


 function exampleparent_copyright() {
     echo "&copy; " . get_bloginfo( 'name' ) . " " . date("Y"); 
 }


 function exampleparent_register_custom_menus() {
     $primary_nav_menu_args = array(
         'primary' => __('Primary Menu', 'exampleparent')
     );
     register_nav_menus($primary_nav_menu_args);
 }

 add_action('after_setup_theme', 'exampleparent_setup');

 function exampleparent_scripts_and_styles() {
     wp_enqueue_style( 'style', get_stylesheet_uri() );
     /**
      * "Better" jQuery inclusion 
      */
      if ( !is_admin() ) {
          wp_deregister_script( 'jquery' );
          wp_register_script( 'jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false); 
          wp_enqueue_script( 'jquery' );
      }
 }
 add_action( 'wp_enqueue_scripts', 'exampleparent_scripts_and_styles' );


 /**
  * Include category IDs in body_class and post_class
  */
  function add_category_classes($classes) {
      global $post;
      foreach( get_the_category($post->ID) as $category ) {
          $classes[] = 'cat-' . $category->slug;
      }
      return $classes;
  }
  add_filter('post_class', 'add_category_classes');

  /**
   * Get the Post slug 
   */
  function get_the_post_slug($id) {
      $post_data = get_post($id, ARRAY_A);
      $slug = $post_data['post_name'];
      return $slug;
  }


  function example_parent_create_blog_post_type() {
      $labels = array(
          'name' => __( 'Blog Posts', 'exampleparent'),
          'singular_name' => __( 'Blog Post', 'exampleparent'),
          'add_new' => __('Add New', 'exampleparent'),
          'add_new_item' => __('Add New Blog Post', 'exampleparent'),
          'edit_item' => __('Edit Blog Post', 'exampleparent'),
          'new_item' => __('New Blog Post', 'exampleparent' ),
          'all_items' => __('All Blog Posts', 'exampleparent'),
          'view_item' => __('View Blog Posts', 'exampleparent'),
          'search_items' => __('Search Blog Posts', 'exampleparent'),
          'not_found' => __('No Blog Posts found', 'exampleparent' ),
          'not_found_in_trash' => __('No Blog Posts found in Trash', 'exampleparent'),
          'parent_item_colon' => __('', 'exampleparent'),
          'menu_name' => __('Blog Posts', 'exampleparent')
      );
      $args = array(
          'labels' => $labels,
          'description' => 'Blog posts for exampleparent',
          'exclude_from_search' => false,
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'show_in_nav_menus' => true,
          'show_in_menu' => true,
          'show_in_admin_bar' => true,
          'query_var' => true,
          'rewrite' => array(
              'slug' => 'blog_post',
          ),
          'has_archive' => true,
          'hierarchical' => false,
          'menu_position' => 20,
          'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
          'can_export' => true
        );
      // ep because exampleparent_postname winds up being too long
      register_post_type('ep_blog_post', $args);
  }