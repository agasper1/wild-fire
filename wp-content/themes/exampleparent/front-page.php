<?php get_header(); ?>
<?php the_post(); ?>
    <article class="page-content">
        <?php the_content(); ?>
    </article>
    <aside class="latest-news">
        <h2>Latest News</h2>
        <?php 
            // New Query for news articles
            $args = array(
                'post_type' => 'post',
                'orderby' => 'data',
                'order' => 'ASC',
                'posts_per_page' => 2 
            ); 
            $latest_news = new WP_Query( $args );
        ?>
        <?php if ( $latest_news->have_posts() ): ?> 
            <?php while ( $latest_news->have_posts() ):  $latest_news->the_post(); ?>
            <?php endwhile; ?> 
        <?php endif; ?> 
        <?php wp_reset_query(); ?>
    </aside>
<?php get_footer(); ?>
