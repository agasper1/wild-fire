<?php 

class Countdown_Timer_Widget extends WP_Widget { 
    // initialize widget values 
    public function __construct() {
        // set base values for the widget, overrloading ?  parent 
        parent::__construct(
            'countdown_timer_widget',
            'CountdownTimer Widget', 
            array(
                'description' => 'A widget that displays your countdown timers', 
            )
        );

        add_action( 'widgets_init', array( $this, 'register_countdown_timer_widgets' ) );
    }

    // handles the "back-end" admin of the widget 
    public function form( $instance ) {
        // collect variables 
        $countdown_timer_id = (isset( $instance['countdown_timer_id'] )) ? $instance['countdown_timer_id'] : 'default'; 
        $number_of_countdown_timers = ( isset( $instance['number_of_countdown_timer_times '] ) ) ? $instance['number_of_countdown_timer_times'] : 1; 
    ?>
    <div class="fieldset"> 
    <!-- Going to phone it in a bit here -->
    <label for="<?php echo $this->get_field_name('countdown_timer_id'); ?>"> Countdown timer to display </label> 
    <select 
        class="form-control-select"
        name="<?php echo $this->get_field_name('countdown_timer_id'); ?>"
        id="<?php echo $this->get_field_id('countdown_timer_id'); ?>"
        value="<?php echo $countdown_timer_id; ?>"
    >
        <option value="default">All countdown timers</option> 
        <?php 
            $args = array(
                'posts_per_page' => -1,
                'post_type' => 'countdown_timer'
            );
            $countdown_timers = get_posts($args); 
        ?> 
        <?php if ($countdown_timers): ?> 
            <?php foreach($countdown_timers as $countdown_timer): ?> 
                <?php if ($countdown_timer === $countdown_timer_id): ?> 
                    <option selected value="<?php echo $countdown_timer->ID;?>"> 
                    <?php get_the_title($countdown_timer->ID); ?>
                    </option> 
                <?php else: ?>
                <option value="<?php echo $countdown_timer->ID;?>"> 
                    <?php get_the_title($countdown_timer->ID); ?>
                </option>  
                <?php endif; ?> 
            <?php endforeach; ?> 
        <?php endif; ?>
    </select>
    </div>

    <?php  
    }
    /**
     * @name update 
     * @description - Handles updating the widget 
     * @param $new_instance - New values 
     * @param $old_instance - Old saved values 
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array(); 

        $instance['countdown_timer_id'] = $new_instance['countdown_timer_id'];
        $instance['number_of_countdown_timer_times'] = $new_instance['number_of_countdown_timer_times']; 


        return $instance; 
    }
    
    /**
     * @name - widget 
     * @description - Handles public display of the widget 
     * @param $args - arguments set by the widget area
     * @param $instance - saved values
     */
    public function widget($args, $instance) {
        $arguments = array(); 

        if ($instance['countdown_timer_id'] !== 'default') {
            $arguments['countdown_timer_id'] = $instance['countdown_timer_id'];
        }

        if ($instance['number_of_countdown_timer_times'] !== 'default') {
            $arguments['number_of_countdown_timer_times'] = $instance['number_of_countdown_timer_times']; 
        }

        $html = '';
        $html .= $args['before_widget'];
        $html .= $args['before_title']; 
        $html .= 'Cant wait to debug this nightmare';
        $html .= $args['after_title'];
        $html .= include( plugin_dir_path( __FILE__ ) . 'templates/countdown_timer.php' );
        $html .= $args['after_widget'];
        echo $html; 
    }

    public function register_countdown_timer_widgets() {
        // presumably tries to go post_type_widget 
        register_widget('countdown_timer_widget');
    }
}