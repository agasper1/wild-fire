<?php 

class Countdown_Timer_Shortcode {
    public function __construct() {
        // register the shortcodes 
        add_action( 'init', array($this, 'register_countdown_timer_shortcodes' ) );
    }

    public function register_countdown_timer_shortcodes() {
        add_shortcode( 'CountdownTimer', array( $this, 'countdown_timer_shortcode_output' ) );       
    }

    public function countdown_timer_shortcode_output($atts, $content = '', $tag) {
        // thinking_face 
        // seems...questionable
        // or is this a humble object example?/could it be
        $countdown_timer = new Countdown_Timer(); 
        // global $countdown_timer; // ??? 
        $shortcode_attributes = array(
            'countdown_timer_id' => '',
            'number_of_countdown_timers' => -1, 
        );
        $arguments = shortcode_atts($shortcode_attributes, $atts, $tag);
        $html = ''; 
        $html .= $countdown_timer->get_countdown_timer_output($arguments);
        wp_enqueue_style('countdown_timer_shortcode_style');
        $debugging = array();
        $debugging['css_stuff'] = array(
            'output' => plugin_dir_url(__FILE__) . 'css/countdown_timer_public_styles.css'
        );
        global $wp_scripts;
        global $wp_styles;
        $debugging['wp_styles'] = array(
            'styles' => $wp_styles
        );
        echo "<pre>";
        print_r($debugging);
        echo "</pre>";
        return $html; 
    }
}

$countdown_timer_shortcode = new Countdown_Timer_Shortcode;