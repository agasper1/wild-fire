<div class="countdown-container-wrapper">
    <h1 class="countdown-timer-label-text">rococo then rubble</h1>
    <div class="countdown-timer">
      <div class="time-denomination">
        <span class="time">{int}</span>
        <span class="label">{string}</span> 
      </div> 
      <div class="time-denomination">
        <span class="time">{int}</span>
        <span class="label">{string}</span> 
      </div> 
      <div class="time-denomination">
        <span class="time">{int}</span>
        <span class="label">{string}</span> 
      </div> 
    </div> 
</div>