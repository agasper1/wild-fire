<section class="meta-data"> 
<?php foreach($meta_data as $meta_data_name => $meta_data_value): ?> 
    <div class="meta-data-<?php echo $meta_data_name; ?>">
        <p class="meta-data-name"><?php echo $meta_data_name; ?></p>
        <p class="meta-data-value"><?php echo $meta_data_value[0]; ?></p>
    </div>
<?php endforeach; ?> 
</section> 