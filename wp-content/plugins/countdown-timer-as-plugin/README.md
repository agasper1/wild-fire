# How do I into custom post type as plugin?
- Great question. 
    - Let's start with the most basic form and work our way up
        - So that means starting with some functional bs and working our way to a class + some type of class + instance method


Let' start with a high level overview of what it is I want:
And let's start with exactly as vague and confusing as it first _feels_: 

- I want to be able to countdown to some time 
- I don't want to countdown to appear on every page 
- I want to be able to countdown to different times on different pages 


Okay where do I go from here?
- Well, I have a vague idea of what the javascript should be like (already too granular)
- How can I store the time I need to countdown to?
- How can I output the countdown?
- What is the countdown?

So while I don't immediately know the answer to all of those questions, I'm beginning to see a theme (pattern, not a theme-theme): 
- I need to be able to load when I am counting down to
    - This (maybe eventually) should scream "custom post type" 
    - I don't want to be writing a million countdown timers that _look_ and _behave_ the same, so how can I abstract this away? 


- Shortcode for the view
    - `[CountdownTimer t="DD::HH::MM::SS"]`



Relevant documentation and odd thoughts 
- [apply_filters](https://developer.wordpress.org/reference/functions/apply_filters/)
    - Why filter? Lets others _extend_ vs. just consume 
- [register_post_type](https://codex.wordpress.org/Function_Reference/register_post_type#Parameters) 
- [nonces](https://codex.wordpress.org/Function_Reference/wp_nonce_field)
    - Nonces. 
- Why flush permalinks?
    - I remember this being a thing to get WP to "realize" something new is there.


TODO 
- [ ] Custom menu icon
- [ ] Bougie usage of something for the front-end
- [ ] Cache 
- [ ] Better way of including html...



post_type_hook_name ? 
countdown_timer_admin_form_end 
wp_location_admin_form_end 


doing an action seems to let people hook into your stuff...

`{post_type}_admin_form_start` 
`{post_type}_admin_form_end` 


```
public function _countdown_timer_shortcode_normalize_attributes($attributes) {
        return array_change_key_case((array)$attributes, CASE_LOWER);
    }

    public function countdown_timer_shortcode($shortcode_attributrs = array(), $content = null, $tag = '') {

        $countdown_timer_attributes = shortcode_atts(
            array(
                'hour' => '',
                'minute' => '',
                'second' => ''
            ),
            'countdown-timer'
        );
        $time_to_countdown_to = explode(":", $countdown_timer_shortcode_attributes['time']);
        countdown_timer_shortcode_html($time_to_countdown_to);

    }

    public function countdown_timer_shortcode_html($time_to_countdown_to) {
        ?>
        <div class="GodIHateStringInterpolationLikeThis">
        <?php print_r($time_to_countdown_to); ?> 
        <p> hello world </p>
        </div>
        <?php 
    }
```



- let "world" know that start of display is starting 
- if (!empty($property_value))
    - do display stuff 



Removed validation check to see if property value was empty 
On the fence. maybe want to signal that the value for a key is emopty rather than not showing it? 


// there's a generic way to do this 
// like array('countdown_timer_label_text', 'countdownm_timer_campaign_id'); 
// foreach($name as $$name) {
    // do validation logic 
    // but I've never been a huge fan of the $$ syntax 
// }


hmmm. paranoia and all, how to use type definition to your advantage for validation checks. 

:thinking_face:: 
```
<?php

$i_cant_stand_typing = array('a', 'b', 'c', 'd');
$how_about_this = array();
foreach ($i_cant_stand_typing as $thing) { 
    $$thing = 'hello world, etc.'; 
    $how_about_this[] = $$thing; 
    if ($thing === 'c') {
        $$thing = '30'; 
    } 
    if ($thing --- 'd') {
        $$thing = 'oh, yeah, how can\'t this go wrong...';
    }
}
echo "how_about_this \n"; 
print_r($how_about_this); 

echo "is c set now: $c \n"; 
echo "how about d: $d"; 

?>
```


Ah, so the location_id thing is probably like 
"Select which location to display" 
turns into (awkwardly named but I didn't pick up in this right away) 
"Select which countdown timer time to display" 

And then the ids are kind of like/could be like  
midnight 
next week 




Let's pretend timezones don't exist for a moment...


- How does this song and dance go again? 
    - _this_ => going from abstract structure to dom as string 
    - Drupal was the same way and if you didn't want to be trying to parse html with regex to manipulate the DOM, you just didn't do certain things at certain times.



## Debugging 
- Well, the admin styles worked... 
- Hmm. something doesn't seem right 
- Hmm `add_meta_boxes` those other properties are odd. Something or another about screens 
- Hmm
```
Warning: call_user_func_array() expects parameter 1 to be a valid callback, class 'Countdown_Timer' does not have a method 'prepend_location_meta_to_content' in /private/var/www/html/ecomm/wild_fire/wordpress/wp-includes/class-wp-hook.php on line 286
```
Let's dig in. 
- Oh, yeah. It doesn't 
    - Updated to `prepend_countdown_timer_meta_to_content`

```
Notice: Array to string conversion in /private/var/www/html/ecomm/wild_fire/wordpress/wp-content/plugins/countdown-timer-as-plugin/templates/countdown_timer_meta.php on line 5
Array
```
Oh word? 

array(
    'countdown_timer' => 'countdown_timer', 
    'meta_properties' => array(
        'campaign_id' => array(
            /* more attributes */ 
        ), 
        'countdown_timer_label_text' => array(
            'value' => 'abc', 
            'validation_rules' => array(
                ?
            )
        )
    )
)

- Slight of hand to not have to deal with this right now 
    - `<p class="meta-data-value"><?php echo $meta_data_value[0]; ?></p>` 
    - `echo $meta_data_value[0]` just goes to show I don't know what I'm doing. If I did, it would just be `echo $meta_data_value`


- Had to update the add_meta_box to not show up on a page (that's not the intent of the plugin)

// think long and hard about the implication of just putting in data when you're not sure where it came from 
Mostly thinking of the $$usage 


- Let's see if I can't get this widget to work
    - Make a page, any page 
    - Some Page 
        - [CountdownTimer /] let's see what explodes


This may be a terrible example for how to code it up? 
Like the concepts are there. But something or another about god objects and SOLID 