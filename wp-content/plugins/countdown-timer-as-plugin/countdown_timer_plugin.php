<?php 

/**
 * @package Countdown_Timer 
 * @version 1.0
 */
/*
Plugin Name: Countdown Timer
Description: For when you need a countdown timer 
Author: Andres Gasper
Version: 1.0
*/ 


class Countdown_Timer {

    private $countdown_timer_times = array();

    public function __construct() {
        // set the default time to countdown to
        add_action( 'init', array( $this, 'set_countdown_timer_times') );
        // register countdown timer countdown time 
        add_action( 'init', array( $this, 'register_countdown_timer_content_type') ); 
        // add meta boxes 
        add_action( 'add_meta_boxes', array( $this, 'add_countdown_timer_meta_boxes' ) ); 
        // save countdown timer 
        add_action( 'save_post_countdown_timer', array( $this, 'save_countdown_timer' ) );


        // admin scripts and styles 
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts_and_styles' ) );

        // public scripts and styles 
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_public_scripts_and_styles' ) );
        
        // public scripts and styles
        add_action( 'init', array( $this, 'register_public_scripts_and_styles' ) ); 

        // gets meta data and displays it before _the_ content ...hm
        add_filter( 'the_content', array( $this, 'prepend_countdown_timer_meta_to_content' ) );

        // activate hook
        register_activation_hook( __FILE__, array( $this, 'plugin_activate' ) );

        // deactivate hook 
        register_deactivation_hook( __FILE__, array( $this, 'plugin_deactivate' ) );

    }

    public function set_countdown_timer_times() {
        // any funny business in having a public function modify a private value...
        $this->countdown_timer_times = apply_filters( 'countdown_timer_times', array(
            'one_hour' => 'One Hour', 
            'midnight' => 'Midnight', 
            'one_week' => 'One Week'
            )
        ); 
    }

    public function register_countdown_timer_content_type() {
        // Define the labels separately because isn't this 'nicer' to read?
        $labels = array(
            'name' => 'Countdown Timer Name', 
            'singular_name' => 'Countdown Timer',
            'menu_name' => 'Countdown Timer', 
            'name_admin_bar' => 'Countdown Time',
            'add_new_item' => 'Add new countdown timer',
            'new_item' => 'New Countdown Timer',
            'edit_item' => 'Edit Countdown Timer',
            'view_item' => 'View Countdown Timer',
            'all_items' => 'All Countdown Timers',
            'search_items' => 'Search Countdown Timers',
            'not_found' => 'No countdown timers found',
            'not_found_in_trash' => 'No countdown timers found in Trash',
        );

        // Arguments for post type 
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav' => true,
            'hierarchical' => false, // I wonder if this is right. Like nest them like Countdown to some day but then roll over to midnight 
            'supports' => array( 'title', 'thumbnail', 'editor' ),
            'has_archive' => false, // lol nope
            'menu_position' => 20,
            'show_in_admin_bar' => true, // sure, why not
            'menu_icon' => '',
            'rewrite' => array(
                'slug' => 'countdown-timer',
                'with_front' => 'true'
            )
        );
         
        register_post_type('countdown_timer', $args);
    }

    public function add_countdown_timer_meta_boxes() {
        // id, name, display function, post type, location, priority 
        add_meta_box(
            'countdown_timer_meta_box', 
            'Countdown Timer Information', 
            array( $this, 'countdown_timer_meta_box_display'),
            'countdown_timer'
        );
    }

    public function countdown_timer_meta_box_display($post) {
        // set nonce field 
        wp_nonce_field( 'countdown_timer_nonce', 'countdown_timer_nonce_field' );

        // collect variables 
        $countdown_timer_label_text = get_post_meta( $post->ID, 'countdown_timer_label_text', true );
        $countdown_timer_campaign_id = get_post_meta( $post->ID, 'countdown_timer_campaign_id', true);
        ?> 
        <p>Enter additional information about the countdown timer</p> 
        <div class="field-container"> 
            <?php do_action( 'countdown_timer_admin_form_start' ); ?> 
            <div class="field">
                <label for="countdown_timer_label_text">Countdown Timer label text</label>
                <input 
                    type="text" 
                    name="countdown_timer_label_text" 
                    id="countdown_timer_label_text"
                    value="<?php echo $countdown_timer_label_text; ?>"
                />
            </div>
            <div class="field">
                <label for="countdown_timer_campaign_id">Countdown Timer Campaign ID</label>
                <input
                    type="number"
                    name="countdown_timer_campaign_id" 
                    id="countdown_timer_campaign_id"
                    value="<?php echo $countdown_timer_campaign_id; ?>"
                />
            </div>
            <?php if( !empty( $this->countdown_timer_times ) ): ?> 
                    <h3>Times to countdown to</h3> 
                    <?php foreach( $this->countdown_timer_times as $time_key => $time_value ): ?> 
                    <?php 
                    $countdown_timer_time_key = "countdown_timer_times_$time_key";
                    $countdown_timer_time_value = get_post_meta( $post->ID, $countdown_timer_time_key, true );
                    ?>
                    <div class="field"> 
                        <label for="<?php echo $countdown_timer_time_key; ?>">
                        <?php echo $countdown_timer_time_key; ?>
                        </label> 
                        <input 
                            type="text" 
                            name="<?php echo $countdown_timer_time_key; ?>" 
                            id="<?php echo $countdown_timer_time_key; ?>" 
                            value="<?php echo $countdown_timer_time_value; ?>" 
                        />
                    </div>
                    <?php endforeach; ?> 
                </div> 
            <?php endif; ?>
            <?php do_action( 'countdown_timer_admin_form_end' ); ?> 
        </div>
        <?php
    }

    // should run only once
    public function plugin_activate() {
        // call the function that creates and registers the countdown timer post type
        $this->register_countdown_timer_content_type();
        // flush permalinks
        flush_rewrite_rules();

    }

    public function plugin_deactivate() {
        $this->log_message('debug', array(
            'deactivated' => 'turning off'
            )
        );
        // flush permalinks 
        flush_rewrite_rules();
    }

    public function get_post_meta_properties($post) {
        $post_meta_properties = array();
        if (isset($post->ID)) {
            $post_meta_properties = get_post_meta($post->ID);
        }
        return $post_meta_properties; 
    }

    public function display_countdown_timer_meta_data($meta_data) {
        // set to the empty string 
        $html = ''; 
        // append waht comes back out of include(file) 
        // questionable use of include's scoping inside of the template file to get this to work
        $html .= include( plugin_dir_path( __FILE__ ) . 'templates/countdown_timer_meta.php' );
        return $html; 
        
    }

    public function prepend_countdown_timer_meta_to_content($content) {
        global $post, $post_type;
        if ($post_type === 'countdown_timer' && is_singular( 'countdown_timer' ) ) {
            // get the meta data for this post 
            $countdown_timer_meta_data = $this->get_post_meta_properties($post);
            // notify the 'world' that this is where the meta data output is starting 
            do_action( 'countdown_timer_meta_data_output_start', $post->ID ); 
            $meta_data_html = $this->display_countdown_timer_meta_data($countdown_timer_meta_data); 
            $content .= $meta_data_html; 
            // notify the 'world' that this is where the meta data output is ending
            do_action( 'countdown_timer_meta_data_output_end', $post->ID); 
        }
        return $content; 
    }

    public function get_countdown_timer_output($arguments = "") {
        // default args
        // hmm...
        $default_args = array(
            'countdown_timer_id' => '',
            'number_of_countdown_timers' => -1
        );

        // update default args if we passed in new args 
        if (!empty( $arguments ) && is_array( $arguments ) ) {
            foreach( $arguments as $argument_key => $argument_value ) {
                if ( array_key_exists( $argument_key, $default_args ) ) {
                    // crude (?) means of validation 
                    // there's something to be said about not re-assigning values
                    $default_args[$argument_key] = $argument_value; 
                }
            }
        }

        // query arguments 
        $countdown_timer_args = array(
            'post_type' => 'countdown_timer',
            'posts_per_page' => $default_args['number_of_countdown_timers'],
            'post_status' => 'publish', 
        );

        // if we passed in a single countdown timer to display
        if (!empty($default_args['countdown_timer_id'])) {
            // questionable 
            $countdown_timer_args['include'] = $default_args['countdown_args'];
        }
        // let me shoot from the hip here for a second 
        // output 
        // questionably useful 
        // so this function can potentially display multiple ?
        // but that kind of fails to ring true here by me not iterating 
        // over the result set and just grabbing the first thing/assuming the 
        // result set has a 'post_content' key

        $countdown_timers = get_posts($countdown_timer_args);
        $countdown_timer_content = apply_filters( 'the_content', $countdown_timers[0]->post_content ); 

        // Classic OBOE 
        $html = '';
        $html .= $countdown_timer_content; 
        $html .= apply_filters('countdown_timer_before_main_content', $html);  
        $html .= include( plugin_dir_path( __FILE__ ) . 'templates/countdown_timer.php' );
        $html = apply_filters( 'countdown_timer_after_main_content', $html ); 

        // return $countdown_timer_output;
        return $html; 
    }

    public function save_countdown_timer($post_id) {
        // big oof on what happens in this function 
        // check for countdown timer nonce 
        if ( !isset( $_POST['countdown_timer_nonce_field'] ) ) {
            // if not found, bail out of the function returning the post id for those downstream 
            return $post_id;
        }
        // check the nonce 
        if ( !wp_verify_nonce($_POST['countdown_timer_nonce_field'], 'countdown_timer_nonce' )) {
            return $post_id;
        }

        // autosave? 
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id; 
        }

        $countdown_timer_label_text = isset( $_POST['countdown_timer_label_text'] ) ? sanitize_text_field( $_POST['countdown_timer_label_text'] ) : '';  
        $countdown_timer_campaign_id = isset( $_POST['countdown_timer_campaign_id'] ) ? sanitize_text_field( $_POST['countdown_timer_campaign_id'] ) : ''; 

        // don't even remember what I called the labels at this point in the code...
        foreach ($_POST as $key => $value) {
            if( preg_match('/^countdown_timer_times_/', $key)) { 
                update_post_meta( $post_id, $key, $value ); 
            }
            // bleh.
            if ( preg_match('/^countdown_timer_label_text/', $key) ) {
                update_post_meta( $post_id, $key, $value ); 
            }
            // bleh.
            if ( preg_match('/^countdown_timer_campaign_id/', $key) ) {
                update_post_meta( $post_id, $key, $value ); 
            }
        }
        do_action('countdown_timer_admin_save', $post_id, $_POST);
        
    }

    public function enqueue_admin_scripts_and_styles() {
        wp_enqueue_style( 'countdown_timer_admin_styles', plugin_dir_url( __FILE__ ) . '/css/countdown_timer_admin_styles.css' );
    }
     /** 
     * @name - log_message 
     * @description - Write to the PHP error log (different/"better" option available?)
     * @param string $level - error | debug | info (?)
     * @param array $data - Data to be serialized and put into the PHP error log
     */
    protected function log_message($level, $data) {
        $message = array(
            'level' => (string) $level, 
            'data' => serialize($data),
            'time_stamp' => date('Y-m-d H:i:sO')
        );
        $serialized_message = serialize($message);
        $formatted_message = "\n $serialized_message \n";
        error_log($formatted_message, 3, 'logs/countdown_timer_plugin_debug.log');
    }

    public function register_public_scripts_and_styles() {
        $this->log_message('debug', array(
            'file_path_for_css' => plugin_dir_url( __FILE__ ) . 'css/countdown_timer_public_styles.css'
            )
        );
        wp_register_style( 'countdown_timer_shortcode_style', plugin_dir_url( __FILE__ ) . 'css/countdown_timer_public_styles.css' );   
    }


    public function enqueue_public_scripts_and_styles() {
        wp_enqueue_style( 'countdown_timer_public_styles', plugin_dir_url( __FILE__) . '/css/countdown_timer_public_styles.css' ); 
        $this->log_message('debug', array(
            'file_path_for_css' => plugin_dir_url( __FILE__ ) . 'css/countdown_timer_public_styles.css'
            )
        );
        // wp_register_style( 'countdown_timer_shortcode_style', plugin_dir_url( __FILE__ ) . 'css/countdown_timer_public_styles.css' );
    }
}

// include shortcodes 
include( plugin_dir_path( __FILE__ ) . 'inc/countdown_timer_shortcode.php' ); 
// include widgets 
include( plugin_dir_path( __FILE__ ) . 'inc/countdown_timer_widget.php' ); 